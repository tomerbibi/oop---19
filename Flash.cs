﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace _2_8_20
{
    public class Flash : Human, IFlash
    {
        public Flash(string name, int age, int voltage) : base(name, age)
        {
            Voltage = voltage;
        }

        public override string Name { get; set; }
        public int Voltage { get; private set; }

        public void ActiveSuperPowers()
        {
            FireLightnings();
        }

        public void FireLightnings()
        {
            Console.WriteLine("starting to throw lighnings");
        }

        public override string ToString()
        {
            return $"name: {Name}, voltage: {Voltage}";
        }
    }
}
