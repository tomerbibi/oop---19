﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace _2_8_20
{
    public abstract class Human
    {
        public abstract string Name { get; set; }
        private int _age;

        public Human(string name, int age)
        {
            Name = name;
            _age = age;
        }



    }
}
