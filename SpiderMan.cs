﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_8_20
{
    public class SpiderMan : Human, IClimb
    {
        public SpiderMan(string name, int age, int speed) : base(name, age)
        {
            Speed = speed;
        }
        public override string Name { get; set; }
        public int Speed { get; private set; }

        public void ActiveSuperPowers()
        {
            Climb();
        }

        public void Climb()
        {
            Console.WriteLine("starting to climb");
        }

        public override string ToString()
        {
            return $"name: {Name}, speed: {Speed}";
        }
    }
}
