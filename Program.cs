﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace _2_8_20
{
    class Program
    {
        public static void ActivateHero(ISuperHero s)
        {
            s.ActiveSuperPowers();
        }
        public static void IdentefyHero(ISuperHero s)
        {
            if (s is SpiderMan)
            {
                Console.WriteLine("spiderman detected");
            }
            else if (s is SuperMan)
            {
                Console.WriteLine("superman detected");
            }
            else if (s is Flash)
            {
                Console.WriteLine("flash detected");
            }
        }
        public static void GetMoreHeroData(ISuperHero s)
        {
            if (s as Flash != null)
            {
                Flash f = s as Flash;
                Console.WriteLine(f.Voltage);
            }
            else if (s as SpiderMan != null)
            {
                SpiderMan SM = s as SpiderMan;
                Console.WriteLine(SM.Speed);
            }
            else if (s as SuperMan != null)
            {
                SuperMan SM = s as SuperMan;
                Console.WriteLine(SM.WebLeft);
            }
        }
        static ISuperHero CreateSuperHero(string SuperHero)
        {
            // i had to create a specific super hero bacause i only get a type of super hero and i dont get the things that i need for the ctor...
            if (SuperHero == "Flash")
            {
                Flash f = new Flash("mord", 34, 778);
                return f;
            }
            else if (SuperHero == "SpiderMan")
            {
                SpiderMan s = new SpiderMan("yoav", 26, 120);
                return s;
            }
            else if (SuperHero == "SuperMan")
            {
                SuperMan s = new SuperMan("shane", 52, 70);
                return s;
            }
            else
            {
                return null;
            }
        }

        static void Main(string[] args)
        {
            SpiderMan jez = new SpiderMan("jez", 22, 90);
            SuperMan jihn = new SuperMan("jihn", 43, 76);
            Flash josh = new Flash("josh", 39, 480);
            ISuperHero[] superHeros = new ISuperHero[3]
            {
                jez,
                jihn,
                josh
            };
            for (int i = 0; i<superHeros.Length; i++)
            {
                superHeros[i].ActiveSuperPowers();
            };
    }
    }
}
