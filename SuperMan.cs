﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_8_20
{
    class SuperMan : Human, IFly
    {


        public SuperMan(string name, int age, int webLeft) : base(name, age)
        {
            WebLeft = webLeft;
        }
        public override string Name { get; set; }
        public int WebLeft { get; private set; }
        public void ActiveSuperPowers()
        {
            Fly();
        }

        public void Fly()
        {
            Console.WriteLine("start flying");
        }
        public override string ToString()
        {
            return $"name: {Name}, web left: {WebLeft}";
        }
    }
}
